/*********************************************************
* Config des images à corriger, leur emplacement
* et la correction à appliquer
* Date: 29-Mai-2021
* Auteur: Yann Roberge
*********************************************************/

#ifndef CONFIG_H
#define CONFIG_H

// Image definition
#define N_IMAGES        5
#define IMG_WIDTH       640
#define IMG_HEIGHT      360

#define IMG_AREA        (IMG_WIDTH * IMG_HEIGHT)
#define IMG_DEPTH       8 // bits
#define IMG_N_CHANNELS  3 // RGB

#define BYTES_PER_PIXEL (IMG_N_CHANNELS * IMG_DEPTH / 8)
#define FRAME_BUF_SIZE  (IMG_AREA * BYTES_PER_PIXEL)

// Files and directories
#define FRAME_IN_DIRECTORY "/run/user/1000/gvfs/sftp:host=odroid,user=nas/media/raid/frames/"
#define FRAME_OUT_DIRECTORY "/run/user/1000/gvfs/sftp:host=odroid,user=nas/media/raid/frames_out/"

#define FRAME_IN_FILENAME "frame"
#define FRAME_OUT_FILENAME "frameout"

// Corrections to apply
#define DELTA_HUE           60
#define DELTA_SATURATION    0.2
#define DELTA_VALUE         0

#endif

