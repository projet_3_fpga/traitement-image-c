CC=gcc
CFLAGS=-ggdb -I.
DEPS = main.h rgb2hsv.h
OBJ = main.o rgb2hsv.o
EXEC = RGB2HSV

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(EXEC): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm -f *.o *~

run: $(EXEC)
	./$(EXEC)

