/*********************************************************
* Conversion d'image RGB­> HSV > RGB
* Date: 24-Mai-2021
* Auteur: Yann Roberge
*********************************************************/

#include "main.h"
#include "rgb2hsv.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

// Load raw image to memory. Images should be in
int loadFrameFromDisk(PixelRGB8* imageRGB, int index) {
    char filepath[100];
    sprintf(filepath, "%s%s%d", FRAME_IN_DIRECTORY, FRAME_IN_FILENAME, index);
    FILE* fd = fopen(filepath, "r");

    int i, status;
    for (i=0; i< IMG_AREA; i++) {
        status = fread((void*) &imageRGB[i], BYTES_PER_PIXEL, 1, fd);
        if (status < 0) {
            fprintf(stderr, "Error while reading %s\n", filepath);
            fclose(fd);
            return status;
        }
    }
    fclose(fd);
    return 0;
}

int writeFrameToDisk(const PixelRGB8* imageRGB, int index) {
    char filepath[100];
    sprintf(filepath, "%s%s%d", FRAME_OUT_DIRECTORY, FRAME_OUT_FILENAME, index);
    FILE* fd = fopen(filepath, "w");

    int i, status;
    for (i=0; i< IMG_AREA; i++) {
        status = fwrite((void*) &imageRGB[i], BYTES_PER_PIXEL, 1, fd);
        if (status < 0) {
            fprintf(stderr, "Error while reading %s\n", filepath);
            fclose(fd);
            return status;
        }
    }
    fclose(fd);
    return 0;
}

int correctPixelColor(PixelRGB8* pixel, int deltaHue, float deltaSaturation, int deltaValue) {
    static PixelHSV tempHSV[1];
    int tempValue;

    // TODO: Implémenter
    printf("Pixel: %d %d %d\n", pixel->red, pixel->green, pixel->blue);

    // Convert to HSV
    rgb2hsv(tempHSV, pixel);

    // DEBUG
    printf("PixelHSV: %d %.4f %d\n", tempHSV->hue, tempHSV->sat, tempHSV->value);

    // Add corrections
    tempHSV->hue = (tempHSV->hue + deltaHue) % 384; // max hue where 64 = 60degrees

    tempHSV->sat = tempHSV->sat + deltaSaturation;
    if (tempHSV->sat > 1.0) {
        tempHSV->sat = 1.0;
    }
    else if (tempHSV->sat < -1.0) {
        tempHSV->sat = -1.0;
    }

    tempValue = (int)tempHSV->value + deltaValue;
    if (tempValue > 255) {
        tempHSV->value = 255;
    }
    else if (tempValue < 0) {
        tempHSV->value = 0;
    }
    else {
        tempHSV->value = tempValue;
    }

    // DEBUG
    //printf("PixelHSV: %d %.4f %d\n", tempHSV->hue, tempHSV->sat, tempHSV->value);

    // Convert back to RGB
    hsv2rgb(pixel, tempHSV);

    //DEBUG
    printf("Pixel reconverti: %d %d %d\n\n", pixel->red, pixel->green, pixel->blue);

    return 0;
}

// Pass hue, saturation and value (lightness / 2) corrections to args
// FIXME: Take args into account
int main(int argc, char* argv) {
    // Start timing
    // TODO: Mesurer le nombre d'image par secondes traitées. Objectif 30fps
    PixelRGB8 imageRGB[IMG_AREA];

    int i, j;
    for (i=0; i<N_IMAGES; i++) {
        loadFrameFromDisk(imageRGB, i);

        // Apply correction to each pixel
        for (j=0; j<IMG_AREA; j++) {
            correctPixelColor(&imageRGB[j], DELTA_HUE,
                                    DELTA_SATURATION, DELTA_VALUE);
        }

        writeFrameToDisk(imageRGB, i);
    }

    // Stop timing
    printf("Done.\n");
    return 0;
}

