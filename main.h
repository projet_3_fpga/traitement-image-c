/*********************************************************
* Conversion d'image RGB­> HSV > RGB
* Date: 25-Mai-2021
* Auteur: Yann Roberge
*********************************************************/

#ifndef MAIN_H
#define MAIN_H

#include <stdint.h>

#include "config.h"

// Pixel struct unpacked for processing
typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t RESERVED;
} PixelRGB8;

// Functions
int loadFrameFromDisk(PixelRGB8* imageRGB, int index);
int writeFrameToDisk(const PixelRGB8* imageRGB, int index);
int correctPixelColor(PixelRGB8* pixel, int deltaHue, float deltaSaturation,
                                        int deltaValue);

#endif

