/*********************************************************
* Conversion d'image RGB­> HSV > RGB
* Date: 24-Mai-2021
* Auteur: Yann Roberge
*********************************************************/

#ifndef RGB2HSV_H
#define RGB2HSV_H

#include "main.h"
#include <stdint.h>

// Image definition

// Pixel struct unpacked for processing
typedef struct {
    // 9-bit hue: 0 to 360 degrees encoded with 60deg = 64
    // on a chromatic circle starting at 60deg (magenta)
    // which avoids modulo operations on some colors
    uint16_t hue;
    float sat;
    uint8_t value;
} PixelHSV;

// Functions
void rgb2hsv(PixelHSV* hsv, PixelRGB8* rgb);
void hsv2rgb(PixelRGB8* rgb, PixelHSV* hsv);

#endif

