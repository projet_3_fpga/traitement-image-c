/*********************************************************
* Conversion d'image RGB­> HSV > RGB
* Date: 24-Mai-2021
* Auteur: Yann Roberge
*********************************************************/

#include "rgb2hsv.h"

// Load raw image to memory

// Convert to HSV

// Add corrections

// Convert back to RGB

// Pass hue, saturation and value (lightness / 2) corrections to args

void rgb2hsv(PixelHSV* hsv, PixelRGB8* rgb) {
    int r = (int) rgb->red;
    int g = (int) rgb->green;
    int b = (int) rgb->blue;
    int minComp, maxComp;

    int tempSubtract, ampl;
    int offset = 64;

    // Min and Max color components
    if (r == 0 && g == 0 && b == 0) {
        hsv->hue = 0;
        hsv->sat = 0;
        hsv->value = 0;
        return;
    }
    else if (r >= g && r >= b) {
        maxComp = r;
        if (g >= b) {
            minComp = b;
        }
        else {
            minComp = g;
        }
        tempSubtract = g - b;
    }
    else if (g >= b) {
        maxComp = g;
        if (r >= b) {
            minComp = b;
        }
        else {
            minComp = r;
        }
        tempSubtract = b - r;
        offset += 128;
    }
    else {
        maxComp = b;
        if (r >= g) {
            minComp = g;
        }
        else {
            minComp = r;
        }
        tempSubtract = r - g;
        offset += 256;
    }

    //H
    ampl = maxComp - minComp;
    if (ampl == 0) {
        // Grayscale color, hue doesn't matter
        hsv->hue = 0;
    }
    else {
        hsv->hue = (tempSubtract*64 / ampl) + offset;
    }

    //S
    if (maxComp == 0) {
        // Pitch black, saturation doesn't matter
        hsv->sat = 0;
    }
    else {
        hsv->sat = 1.0*ampl / maxComp;
    }

    //V
    hsv->value = maxComp;
}

void hsv2rgb(PixelRGB8* rgb, PixelHSV* hsv) {
    //TODO: Implémenter
    int hueMSB = (int)hsv->hue & 0x1C0;
    int hueLSB = (int)hsv->hue & 0x03F;

    int tempVS = hsv->value * hsv->sat;
    int vMinusVS = hsv->value - tempVS;
    int evenMSB = hsv->value - ( (tempVS * hueLSB) / 0x40);
    int oddMSB = hsv->value - ( (tempVS * (0x40 - hueLSB)) / 0x40);

    // Formulas for HueMBS shifted down by 1
    // so as to take the hue offset into account
    // (equation 6.69 in Bailey does not)
    switch (hueMSB >> 6) {
        case 0:
            rgb->red = hsv->value;
            rgb->green = vMinusVS;
            rgb->blue = evenMSB;
            break;
        case 1:
            rgb->red = hsv->value;
            rgb->green = oddMSB;
            rgb->blue = vMinusVS;
            break;
        case 2:
            rgb->red = evenMSB;
            rgb->green = hsv->value;
            rgb->blue = vMinusVS;
            break;
        case 3:
            rgb->red = vMinusVS;
            rgb->green = hsv->value;
            rgb->blue = oddMSB;
            break;
        case 4:
            rgb->red = vMinusVS;
            rgb->green = evenMSB;
            rgb->blue = hsv->value;
            break;
        case 5:
            rgb->red = oddMSB;
            rgb->green = vMinusVS;
            rgb->blue = hsv->value;
            break;
    }
}

